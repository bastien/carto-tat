---
title: Mentorship
id: "tat-mentorship"
types:
  - program
---

Tech Against Terrorism a un programme de mentorship. L’objectif est d’aider les petites plateformes dans leur modération. D’après le site de la Online Harms Foundation, ce mentorship est fait par le [[gifct|GIFCT]]. On peut imaginer que l’idée est d’inciter les plateformes du programme à utiliser la plateforme Terrorist Content Analitics Platform.