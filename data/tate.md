---
title: Tech Against Terrorism Europe
id: "tate"
types:
  - organisation
---

Tech Against Terrorism Europe (TATE) est la branche européenne de [[tat|Tech Against Terrorism]]. C’est un consortium lancé en 2023, avec un événement de lancement à l’Assemblée nationale organisé par Braun-Pivet. Le financement est assuré par l’[[ue|Union européenne]]. TAT n’a pas l’air de financer TATE mais est bien en lead du consortium.

L’objectif officiel de TATE est d’aider les petits hébergeurs à se mettre en conformité avec le TERREG. Sauf qu’on voit avec leur rapport de janvier que l’objectif officieux semble être de pousser une certaine forme de mise en conformité, à base d’analyse algorithmique des contenus.

Font partie du consortium de TATE :

- [[tat|Tech Against Terrorism]] ;
- [[saher-europe|SAHER Europe]] ;
- la [[dcu|Dublin City University]] ;
- l’[[univ-gand|université de Gand]] (Belgique) ;
- le [[jos||projet JOS]] ;
- la [[lmum|Ludwig-Maximilians-Universität München]] ;
- le [[cytrec|centre de recherche sur les cybermenaces]] (CYTREC) de l’université de Swansea (Royaume-Uni)

Certaines personnes du consortium ont également des liens avec GNET, la branche « recherche » du GIFCT.

TATE a produit en 2024 un rapport en faveur du TERREG et des dispositifs d'analyse automatisée des contenus ([@tate2024]).