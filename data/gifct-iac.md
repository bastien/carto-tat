---
title: "Independent Advisory Committee"
id: "gifct-iac"
types:
  - committee
---

Le Independent Advisory Committee du [[gifct|GIFCT]] est composé de représentant·es de gouvernements, d’organisations de la société civile et d’universitaires. On retrouve le Canada via leur pôle [[securite-publique-canada|Sécurité publique Canada]], mais aussi la [[france|France]], le [[japon|Japon]], la [[nouvelle-zelande|Nouvelle-Zélande]], le [[royaume-uni|Royaume-Uni]] et les [[etats-unis|États-Unis]]. On retrouve également l’[[ue|Union européenne]] et la direction Anti-terro des Nations Unies.
