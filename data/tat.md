---
title: Tech Against Terrorism
id: "tat"
types:
  - organisation
---

# Tech Against Terrorism

[Tech Against Terrorism](https://techagainstterrorism.org/) se dit être une ONG défendant l’usage éthique de la technologie pour contrer les contenus terroristes en ligne. Ce n’est pas évident de savoir qui est derrière. Leur [page « About »](https://techagainstterrorism.org/about) est très floue : elle met en avant le fait que cette initiative aurait débuté en 2016 aux Nations Unies, et qu’il s’agit d’un « partenariat public-privé indépendant ».