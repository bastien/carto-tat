---
title: Terrorist Content Analitics Platform
id: "tcap"
types:
  - platform
---

Il s’agit d’une plateforme d’analyse automatisée des contenus pour repérer du contenu terroriste. C’est la concrétisation de l’objectif politique de TAT : automatiser la modération des contenus. Le projet est co-financé par le [[securite-publique-canada|gouvernement canadien]] et [[tat|Tech Against Terrorism]] (je n’ai pas trouvé dans quelles proportions).

Google a lancé en 2023 [[altitude|Altitude]], qui a l’air d’être un outil clé en main pour interroger gratuitement la plateforme TCAP.