---
title: Global Network on Extremism and Technology
id: "gnet"
types:
  - research
---

Le [[gifct|GIFCT]] finance un programme de recherche sur les contenus extrémistes, le Global Network on Extremism and Technology (GNET). Le GNET est une sorte de sous-projet de l’[[icsr|International Centre for the Study of Radicalisation (ICSR)]].