---
title: Board
id: "gifct-board"
types:
  - committee
---



Le board du [[gifct|Global Internet Forum to Counter Terrorism]] est composé de « *personnes issues des entreprises fondatrices : [[facebook|Facebook]], [[microsoft|Microsoft]], [[twitter|X (anciennement Twitter)]], et [[youtube|Youtube]]* ». Dans le [rapport d’activités 2022](https://gifct.org/wp-content/uploads/2022/12/GIFCT-Annual-Report-2022.pdf) du GIFCT, on peut voir que leur budget est d’un peu plus de $4M par an et on peut voir dans les plus petits financeurs [[discord|Discord]], [[dropbox|Dropbox]], [[airbnb|Airbnb]], [[zoom|Zoom]], [[amazon|Amazon]], [[pinterest|Pinterest]]. [[giphy|Giphy]], [[niantic|Niantic]] et [[clubhouse|Clubhouse]] sont également membres depuis 2022. Mais aucun État dans le board ni dans les financeurs.