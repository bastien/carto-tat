---
title: Centre de recherche sur les cybermenaces
id: "cytrec"
types:
  - research
---

Centre de recherche sur les cybermenaces (CYTREC) de l’[[univ-swansea|université de Swansea]] (Royaume-Uni)