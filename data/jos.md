---
title: Projet JOS
id: "jos"
types:
  - research
---

Un truc un peu fumeux, sans site internet, qui se présente comme « *surveill*[ant] *les contenus extrémistes - sur Internet et dans les médias sociaux* ».