FROM nginx:mainline

LABEL maintainer="blq@laquadrature.net"

WORKDIR /app

ENV DEBIAN_FRONTEND=noninteractive

RUN apt update && apt install -y npm make && npm install @graphlab-fr/cosma -g
RUN cosma modelize --citeproc

COPY export/cosmoscope.html /usr/share/nginx/html/index.html
